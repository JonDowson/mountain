     .
                           _   _ _______    _       
                          | \ | |__   __|  (_)      
     _ __ ___   ___  _   _|  \| |  | | __ _ _ _ __  
    | '_ ` _ \ / _ \| | | | . ` |  | |/ _` | | '_ \
    | | | | | | (_) | |_| | |\  |  | | (_| | | | | |
    |_| |_| |_|\___/ \__,_|_| \_|  |_|\__,_|_|_| |_|


## **Authors**

- _jon dowson_

-----------------------

## **About**

- `mouNTain` is a terminal application written in `bash`
- manage with ease the mounting / unmounting of multiple remote file systems
- tested to work-on and connect-to machines running `ubuntu`, `centos` and `mac`  
- useful features at your fingertips:  
    - edit remote files with your favourite local editor e.g. `atom`
    - scan networks for devices / docker containers
    - discover ip addresses and port status

-----------------------

## **Prerequisites**

- support for bash associative arrays:  
    - bash 4.0 or higher
- os `mac`:  
    - fuse and fuse-sshfs
    - homebrew package manager:
        - core-utils
        - arp-scan
        - latest version of bash 4.x
- os `ubuntu` and `centos`:  
    - arp-scan

-----------------------

## **Setup**

**1. clone the `mouNTain` repository**

```bash
$ git clone git@bitbucket.org:JonDowson/mountain.git
```

**2. run `setup_mountain.sh`**

```bash
$ cd mountain
$ ./lib/setup_mouNTain.sh
```
the `setup_mountain.sh` script will:  

- rename the folder from `mountain` to `mouNTain`    
- install all required 3rd party libraries if not already present  
    - os `ubuntu` + `centos`:
        - `arp-scan` as a package install
    - os `mac`:
        - `homebrew` which itself will install `core-utils`, `arp-scan` and `bash-4.x`
        - the end of the script will instruct you to:
            - install `fuse` and `fuse-sshfs` manually
            - select the homebrew version of bash as default for system 

**3. enable `mouNTain` to be run by `sudo` from any folder path**

```bash
$ sudo visudo
```
then add the path of the mouNTain folder to the `secure_path`
```bash
Defaults secure_path="<existing_path>:</path/to/mouNTain>"
```

-----------------------

## **Usage**

**1. start new terminal and run `mouNTain` from any directory**

```bash
$ sudo mountain -h
```
```bash
==> mouNTain commands:

.. help:                          $ sudo mountain -h
.. list all mounts:               $ sudo mountain -l
.. mount:                         $ sudo mountain -m <user@ip>
.. mount (specify key):           $ sudo mountain -m:</path/to/key> <user@ip>
.. scan local machine for nics:   $ sudo mountain -s
.. scan for devices on nic:       $ sudo mountain -s:<nic>
.. scan all ports on this device: $ sudo mountain -s::<ipToScan>_*
.. scan this port on this device: $ sudo mountain -s::<ipToScan>_<portToScan>
.. unmount specific mount:        $ sudo mountain -u <user@ip>
.. unmount all mounts:            $ sudo mountain -ua

==> mouNTain usage:

(1) Once connected to remote fs, 'cd' to its mounted folder

$ cd mouNTain/mounts/<user@ip>

OR use local editor, e.g. atom, to select from and manage multiple mounted folders

(2) Then work with remote files (usual permissions apply)

--> files executed on the mount will run locally !!
--> wake any mounted devices from sleep to assist umount !!

==> mouNTain authentication:

First checks for secure keys else a password is required:

To setup a secure key for remote access:

(1) create an ssh-key if one does not already exist for local machine

$ ssh-keygen -t rsa

(2) use 'ssh-copy-id' to transfer it securely to the remote machine

$ ssh-copy-id <user@ip>
```

-----------------------

## **Updates**

- future enhancements will be added as version branches
- the master branch will always be the latest stable version

to check which branch you are using:
```bash
$ git branch
```
to sync with repo:
```bash
$ git pull
```
to change which branch you are using:
```bash
$ git checkout <branch>
```
