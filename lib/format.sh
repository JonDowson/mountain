#!/bin/bash

# script_name: format.sh
# author: jondowson
# about: formatting for screen output for the mouNTain application

#-------------------------------------------

# Setup colors and text effects
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
b=`tput bold`
u=`tput sgr 0 1`
ul=`tput smul`
xl=`tput rmul`
stou=`tput smso`
xtou=`tput rmso`
reverse=`tput rev`
reset=`tput sgr0`

tick="${b}${green}✓${reset}"
cross="${b}${red}✘${reset}"
