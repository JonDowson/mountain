#!/bin/bash

# script_name: functions.sh
# author: jondowson
# about: functions used by the mouNTain application

#-------------------------------------------

# use sshfs to mount a remote files system to a local folder
function mnt_mount(){

mkdir -p "${mnt_here}"
sshfs -o allow_other,IdentityFile=${keyPath} "${who_where}:/" "${mnt_here}"
if [[ $? != 0 ]]; then
  msg_mnt_mountFailed
  exit 1;
fi
}

#-------------------------------------------

# check for existence of mounted folder (before any attempt to unmount)
function mnt_check_unmount(){

umnt_there="${mountainFolder}/mounts/${who_where}"
if [[ ${who_where} ]]; then
  if [[ ! $(mount | grep mouNTain) == *"${umnt_there}"* ]] && [[ ! -d "${umnt_there}" ]]; then
    msg_mnt_noMountFound
    msg_mnt_list
    exit 1;
  fi
else
  msg_mnt_noMountSpecified
  msg_mnt_list
  exit 1;
fi
}

#-------------------------------------------

# attempt to unmount a given mounted folder and remove its local folder
function mnt_unmount_one(){

umnt_there="${mountainFolder}/mounts/${who_where}"
if [ -d "${umnt_there}" ]; then
  try=$(umount ${umnt_there} 2>&1 1>/dev/null)
  if [[ $try == *"not mounted"* ]] || [[ $try == *"not currently mounted"* ]]; then
    rm -rf ${umnt_there}
  elif [[ $try == *"busy"* ]]; then
    msg_mnt_pathUnmountCheck
    exit 1;
  else
    rm -rf ${umnt_there}
  fi
else
  try=$(umount ${umnt_there} 2>&1 1>/dev/null)
  if [[ $try == *"busy"* ]]; then
    msg_mnt_pathUnmountCheck
    exit 1;
  fi
  tryHarder=$(umount -f "${umnt_there}" 2>&1 1>/dev/null)
  rm -rf "${umnt_there}"
fi
msg_mnt_list
msg_mnt_notListedAbove
}

#-------------------------------------------

# attempt to unmount all mounted folder(s) and remove their local folder(s)
function mnt_unmount_all(){

dirs=($(find ${mountainFolder}/mounts -maxdepth 1 -type d))
for umnt_there in "${dirs[@]:1}"; do
  if [ -d "${umnt_there}" ]; then
    try=$(umount ${umnt_there} 2>&1 1>/dev/null)
    if [[ $try == *"not mounted"* ]] || [[ $try == *"not currently mounted"* ]]; then
      rm -rf ${umnt_there}
    elif [[ $try == *"busy"* ]]; then
      msg_mnt_pathUnmountCheck
      exit 1;
    else
      tryHarder=$(umount -f "${umnt_there}" 2>&1 1>/dev/null)
      rm -rf "${umnt_there}"
    fi
  else
    try=$(umount ${umnt_there} 2>&1 1>/dev/null)
    if [[ $try == *"busy"* ]]; then
      echo e
      msg_mnt_pathUnmountCheck
      exit 1;
    fi
    tryHarder=$(umount -f "${umnt_there}" 2>&1 1>/dev/null)
    rm -rf "${umnt_there}"
  fi
done
msg_mnt_list
msg_mnt_notAnyListedAbove
}

#-------------------------------------------

# print mouNTain help to screen
function mnt_help(){

msg_mnt_mntExamples
msg_mnt_mntInstructions
msg_mnt_keysPassword
}

#-------------------------------------------

# check setup script has been run with root user permissions
function mnt_sudo_check_setup(){

if [[ $UID != 0 ]]; then
  clear
  msg_mnt_sudoCheckSetup
  exit 1;
fi
}

#-------------------------------------------

# check mountain script has been run with root user permissions
function mnt_sudo_check(){

if [[ $UID != 0 ]]; then
  clear
  msg_mnt_sudoCheck
  exit 1;
fi
}

#-------------------------------------------

# find local network interface cards
function mnt_scan_network(){

msg_mnt_networkCardsDetected
if [[ ${os} == "Mac" ]]; then
  ifconfig -l
elif [[ ${os} == "Ubuntu" ]] || [[ ${os} == "Centos" ]] || [ ${os} == "Redhat" ]]; then
  ifconfig -s
fi
printf "%s\n"
msg_mnt_networkCardsTip
}

#-------------------------------------------

# scan given network interface card for ip addresses of devices
function mnt_scan_devices(){

msg_mnt_networkDevices
arp-scan --interface=${nic} --localnet
printf "%s\n"
msg_mnt_networkCardsTip
}

#-------------------------------------------

# for a given ip address, scan a given port or all of them
function mnt_scan_device_nmap(){

if [[ ${nmapIpRaw} != *"_"* ]]; then
    msg_mnt_nmapDeviceBadFlag
    exit 1;
else
  if [[ ${nmapPort} == "*" ]]; then
    msg_mnt_nmapDevice
    nmap ${nmapIp} -Pn
  elif [[ "${nmapPort}" -ge 0 ]] && [[ "${nmapPort}" -le 65535 ]] && [[ "${nmapPort}" != "" ]]; then
    msg_mnt_nmapDevice
    nmap ${nmapIp} -Pn -p ${nmapPort}
  else
    msg_mnt_nmapDeviceBadFlag
    exit 1;
  fi
fi
printf "%s\n"
msg_mnt_networkCardsTip
}

#-------------------------------------------

# if docker is installed detect ip addresses of running containers
function mnt_scan_docker(){

docker > /dev/null 2>&1
if [[ "$?" == 0  ]]; then
  msg_mnt_dockerContainersList
  docker inspect -f '{{.Name}} - {{.NetworkSettings.IPAddress }}' $(docker ps -aq)
  printf "%s\n"
fi
}
