#!/bin/bash

# script_name:   functions_generic.sh
# author:        jondowson
# about:         generic bash functions usable by any script

# ---------------------------------------

function msg_colour_simple(){

## display messages based on a simple colour scheme

megType="${1}"
message="${2}"

case ${1} in
    "title" )
        printf "%s\n\n" "${b}${cyan}==> ${message}:${reset}" ;;
    "alert" )
        printf "%s\n"   "${b}${yellow}--> ${message} !!${reset}" ;;
    "warning" )
        printf "%s\n"   "${b}${red}--> ${message} !!${reset}" ;;
    "info" )
        printf "%s\n"   "${message} ${reset}" ;;
    "info-bold" )
        printf "%s\n"   "${b}${message} ${reset}" ;;        
esac
}
