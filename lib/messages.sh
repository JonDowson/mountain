#!/bin/bash

# script_name: messages.sh
# author: jondowson
# about: functions with terminal messages for the mouNTain application

#-------------------------------------------

function msg_mnt_keysPassword(){

printf "%s\n\n" "${b}${cyan}==> mouNTain authentication:${reset}"
printf "%s\n\n" "First checks for secure keys else a password is required:"
printf "%s\n\n"   "To setup a secure key for remote access:"
printf "%s\n\n"   "${b}(1) create an ssh-key if one does not already exist for local machine${reset}"
printf "%s\n\n"   "$ ssh-keygen -t rsa"
printf "%s\n\n"   "${b}(2) use 'ssh-copy-id' to transfer it securely to the remote machine${reset}"
printf "%s\n\n" "$ ssh-copy-id <user@ip>"
}

#-------------------------------------------

function msg_mnt_mntExamples(){

printf "%s\n\n" "${b}${cyan}==> mouNTain commands:${reset}"
printf "%s\n"   ".. help:                          $ sudo mountain -h"
printf "%s\n"   ".. list all mounts:               $ sudo mountain -l"
printf "%s\n"   ".. mount:                         $ sudo mountain -m <user@ip>"
printf "%s\n"   ".. mount (specify key):           $ sudo mountain -m:</path/to/key> <user@ip>"
printf "%s\n"   ".. scan local machine for nics:   $ sudo mountain -s"
printf "%s\n"   ".. scan for devices on nic:       $ sudo mountain -s:<nic>"
printf "%s\n"   ".. scan all ports on this device: $ sudo mountain -s::<ipToScan>_*"
printf "%s\n"   ".. scan this port on this device: $ sudo mountain -s::<ipToScan>_<portToScan>"
printf "%s\n"   ".. unmount specific mount:        $ sudo mountain -u <user@ip>"
printf "%s\n\n" ".. unmount all mounts:            $ sudo mountain -ua"
}

#-------------------------------------------

function msg_mnt_mntInstructions(){

printf "%s\n\n" "${b}${cyan}==> mouNTain usage:${reset}"
printf "%s\n\n"   "${b}(1) Once connected to remote fs, 'cd' to its mounted folder${reset}"
printf "%s\n\n"   "$ cd mouNTain/mounts/<user@ip>"
printf "%s\n\n"   "OR use local editor, e.g. atom, to select from the mounted folder"
printf "%s\n\n" "${b}(2) Then work with remote files (usual permissions apply)${reset}"
printf "%s\n"   "${b}${yellow}--> files executed on the mount will run locally !!${reset}"
printf "%s\n\n" "${b}${yellow}--> wake any mounted devices from sleep to assist umount !!${reset}"
}

#-------------------------------------------

function msg_mnt_sudoCheckNic(){

printf "%s\n\n" "${b}${b}${red}==> mouNTain network scan must be run with sudo !!${reset}"
printf "%s\n\n" "$ sudo mountain -s:<nic>"
printf "%s\n\n" "${b}${yellow}--> use just '-s' flag to see list of nics !!${reset}"
}

#-------------------------------------------

function msg_mnt_sudoCheck(){

printf "%s\n\n" "${b}${b}${red}==> mouNTain needs to be run with sudo !!${reset}"
printf "%s\n\n" "Prepend each command with path to mountain, e.g:"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -h"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -l"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -m <user@ip>"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -m:</path/to/key> <user@ip>"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -s"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -s:<nic>"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -s::<ipToScan>_*"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -s::<ipToScan>_<portToScan>"
printf "%s\n"   "$ sudo ${mountainFolder}/mountain -u <user@ip>"
printf "%s\n\n" "$ sudo ${mountainFolder}/mountain -ua"
printf "%s\n\n" "${b}${yellow}--> or avoid using path with sudo by appending to root's secure_path !!${reset}"
printf "%s\n\n" "$ sudo visudo"
printf "%s\n\n" "Then append to secure_path:"
printf "%s\n\n" "Defaults secure_path=<existing_path>:${yellow}${mountainFolder}${reset}"
}

#-------------------------------------------

function msg_mnt_sudoCheckSetup(){

printf "%s\n\n" "${b}${b}${red}==> mouNTain needs to be installed with sudo !!${reset}"
printf "%s\n\n"   "$ sudo ./lib/setup_mountain.sh"
}

#-------------------------------------------

function msg_mnt_pathUnmountCheck(){

printf "%s\n\n" "${b}${red}==> Leave any mounted mouNTain folder path before deleting mount !!${reset}"
printf "%s\n"   "First save any open work that is using the mount !!"
printf "%s\n\n" "Close all terminal tabs / windows using mount path !!"
printf "%s\n"   "${b}${yellow}--> editors using disconnected mounts will save locally to the mount folder !!${reset}"
printf "%s\n\n" "${b}${yellow}--> wake any mounted devices from sleep to assist umount !! ${reset}"
}

#-------------------------------------------

function msg_mnt_mountTip(){

printf "%s\n\n" "${b}${green}==> Folder is now mounted to the remote machine !!${reset}"
printf "%s\n\n" "${b}Manage all mounted folder(s) directly using your favourite editor:${reset}"
printf "%s\n\n" "$ atom ${mountainFolder}/mounts"
printf "%s\n\n" "${b}OR copy this to navigate using the terminal:${reset}"
printf "%s\n\n" "$ cd ${mnt_here} && ls -la"

}

#-------------------------------------------

function msg_mnt_list(){

printf "%s\n\n" "${b}${cyan}==> mouNTain folders:${reset}"
printf "%s\n\n" "$ ls -la ${mountainFolder}/mounts/"
ls -la "${mountainFolder}/mounts/"
printf "%s\n"
printf "%s\n\n" "${b}${green}==> mounted mouNTain folders:${reset}"
printf "%s\n\n" "$ mount | grep mouNTain"
mount | grep mouNTain
printf "%s\n"
}

#-------------------------------------------

function msg_mnt_badFlags(){

printf "%s\n\n" "${b}${red}==> Bad or missing flag(s) !!${reset}"
printf "%s\n\n" "Now running mouNTain help:"
printf "%s\n\n" "$ sudo mountain -h"
mnt_help
}

#-------------------------------------------

function msg_mnt_osNotSupported(){

printf "%s\n\n" "${b}${red}==> mouNTain does not support this OS !!${reset}"
}

#-------------------------------------------

function msg_mnt_mountFailed(){

printf "%s\n\n" "${b}${red}==> Mount failed !!${reset}"
}

#-------------------------------------------

function msg_mnt_notListedAbove(){

printf "%s\n\n" "${b}${yellow}--> ${who_where} should not be listed above !!${reset}"
}

#-------------------------------------------

function msg_mnt_notAnyListedAbove(){

printf "%s\n\n" "${b}${yellow}--> No folders or mounts should be listed above !!${reset}"
}

#-------------------------------------------

function msg_mnt_noMountSpecified(){

printf "%s\n\n" "${b}${red}==> No mount specified !!${reset}"
printf "%s\n\n" "Specify an existing mount to unmount !!${reset}"
}

#-------------------------------------------

function msg_mnt_noMountFound(){

printf "%s\n\n" "${b}${red}==> The mount specified does not exist !!${reset}"
printf "%s\n\n" "Specify an existing mount to unmount !!${reset}"
}

#-------------------------------------------

function msg_mnt_networkCardsDetected(){

printf "%s\n\n" "${b}${cyan}==> Detected network interface cards:${reset}"
}

#-------------------------------------------

function msg_mnt_networkCardsTip(){

printf "%s\n\n" "${b}${yellow}--> use mouNTain to scan: nics / devices / ports:${reset}"
printf "%s\n"   "$ sudo mountain -s                            (scan for local network interface cards)"
printf "%s\n"   "$ sudo mountain -s:<nic>                      (scan for devices on a network interface card)"
printf "%s\n"   "$ sudo mountain -s::<ipToScan>_*              (scan most common ports on the device with this ip)"
printf "%s\n\n" "$ sudo mountain -s::<ipToScan>_<portToScan>   (scan the device on this port)"
}

#-------------------------------------------

function msg_mnt_networkDevices(){

printf "%s\n\n" "${b}${cyan}==> Devices detected on network card ${yellow}${nic}:${reset}"
}

#-------------------------------------------

function msg_mnt_dockerContainersList(){

printf "%s\n\n" "${b}${cyan}==> Detected local docker containers:${reset}"
}

#-------------------------------------------

function msg_mnt_nmapDevice(){

if [[ ${nmapPort} == "*" ]]; then
  printf "%s\n\n" "${b}${cyan}==> Probing 1000 most commonly used ports on ${yellow}${nmapIp}${reset}"
  printf "%s\n\n" "$ nmap ${nmapIp} -Pn"
  printf "%s\n" "Hit <enter> to see progress>"
else
  printf "%s\n\n" "${b}${cyan}==> Probing port ${yellow}${nmapPort}${cyan} on ${yellow}${nmapIp}${reset}"
  printf "%s\n\n" "$ nmap ${nmapIp} -Pn -p ${nmapPort}"
  printf "%s\n" "Hit <enter> to see progress>"
fi
}

#-------------------------------------------

function msg_mnt_nmapDeviceBadFlag(){

printf "%s\n\n" "${b}${red}==> The port scan flag is malformed !!${reset}"
printf "%s\n"   "$ sudo mountain -s::<ip>_*             (scan most commonly used 1000 ports)"
printf "%s\n\n" "$ sudo mountain -s::<ip>_<portToScan>  (scan a port between 0 and 65535)"
}
