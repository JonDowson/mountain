#!/bin/bash

# script_name: setup_mountain.sh
# author: jondowson
# about: script to install dependencies for the mouNTain sshfs terminal application

#-------------------------------------------

# uncomment for full trace
# set -x

#-------------------------------------------

clear

# determine host OS
os=$(uname -a)
if [[ ${os} == *"Darwin"* ]]; then
  os="Mac"
elif [[ ${os} == *"Ubuntu"* ]]; then
  os="Ubuntu"
elif [[ "$(cat /etc/system-release-cpe)" == *"centos"* ]]; then
  os="Centos"
elif [[ "$(cat /etc/system-release-cpe)" == *"redhat"* ]]; then
  os="Redhat"
else
  os="bad"
fi

#-------------------------------------------

# get all the mouNTain scripts and source them so that their functions are available
# .. don't bother sourcing the mountain setup script
files="$(find lib/ -name "*.sh*" | grep -v  "setup_mountain.sh")"
for file in $(printf "%s\n" "$files"); do
    [ -f $file ] && . $file
done

#-------------------------------------------

msg_colour_simple "title" "Checking mouNTain dependencies:"
# rename folder 'mountain' to 'mouNTain'
if [[ -d ../mountain ]]; then
  msg_colour_simple "alert" "Ensuring 'mountain' folder is spelt 'mouNTain'"
  mv ../mountain ../mouNTain
fi

#-------------------------------------------

if [[ ${os} == "Mac" ]]; then

  brewList=$(brew list)

  if [[ $brewList == *"command not found"* ]]; then
    msg_colour_simple "alert" "Installing homebrew"
  else
    msg_colour_simple "alert" "Fetching latest homebrew"
  fi
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  printf "%s\n"
  
  if [[ $brewList == *"coreutils"* ]]; then
    msg_colour_simple "alert" "Fetching latest core-utils"
    printf "%s\t" "$ brew upgrade coreutils"
    brew upgrade coreutils > /dev/null 2>&1
    printf "%s\n" "${tick}"
  else
    msg_colour_simple "alert" "Installing core-utils"
    printf "%s\t" "$ brew install coreutils"
    brew install coreutils > /dev/null 2>&1
    printf "%s\n" "${tick}"
  fi

  if [[ $brewList == *"arp-scan"* ]]; then
    msg_colour_simple "alert" "Fetching latest arp-scan"
    printf "%s\t\t" "$ brew upgrade arp-scan"
    brew upgrade arp-scan > /dev/null 2>&1
    printf "%s\n" "${tick}"
  else
    msg_colour_simple "alert" "Installing arp-scan"
    printf "%s\t\t" "$ brew install arp-scan"
    brew install arp-scan > /dev/null 2>&1
    printf "%s\n" "${tick}"
  fi

  if [[ $brewList == *"bash"* ]]; then
    msg_colour_simple "alert" "Fetching latest bash"
    printf "%s\t\t" "$ brew upgrade bash"
    brew upgrade bash > /dev/null 2>&1
    printf "%s\n" "${tick}"
  else
    msg_colour_simple "alert" "Installing bash"
    printf "%s\t\t" "$ brew install bash"
    brew install bash > /dev/null 2>&1
    printf "%s\n" "${tick}"
  fi
  printf "%s\n"
  msg_colour_simple "alert" "Homebrew installed packages:"
  msg_colour_simple "info"  "$ brew list"
  brew list
  printf "%s\n"

  msg_colour_simple "title" "Final tasks to setup mouNTain:"
  msg_colour_simple "info-bold" "(1) Install Fuse and SSHFS for Mac from:"
  msg_colour_simple "info" "https://osxfuse.github.io/" 
  printf "%s\n"
  msg_colour_simple "info-bold" "(2) Enable sudo to run mouNTain from any folder:"
  msg_colour_simple "info"  "$ sudo visudo"
  msg_colour_simple "info"  ".. add line: 'Defaults secure_path:<existing_path>:/path/to/mouNTain'"
  printf "%s\n"
  msg_colour_simple "info-bold" "(3) Select default bash for mac: (no need if bash version is > 4)"
  msg_colour_simple "info" "(a) Open settings --> Users&Groups --> right-click --> advanced options"
  msg_colour_simple "info" "(b) Change login shell to use /usr/local/bin/bash (i.e. point to homebrew version of bash)" 
  msg_colour_simple "info" "(c) start new terminal and check bash version --> $ bash --version"
  printf "%s\n"
  msg_colour_simple "info-bold"  "(4) Run mouNTain help for usage instructions:"
  msg_colour_simple "info"  "$ sudo mountain -h"
  printf "%s\n"

elif [[ ${os} == "Ubuntu" ]]; then

  msg_colour_simple "alert" "Getting apt package manager update"
  printf "%s\t\t" "$ sudo apt-get -y update"
  sudo apt-get -qq -y update > /dev/null 2>&1
  printf "%s\n" "${green}${b} ✓${reset}  $1${reset}"
  msg_colour_simple "alert" "Installing sshfs package"
  printf "%s\t\t" "$ sudo apt-get -y install sshfs"
  sudo apt-get install -qq -y sshfs
  printf "%s\n" "${green}${b} ✓${reset}  $1${reset}"
  msg_colour_simple "alert" "Installing arp-scan"
  printf "%s\t"   "$ sudo apt-get -y install arp-scan"
  sudo apt-get install -qq -y arp-scan
  printf "%s\n\n" "${green}${b} ✓${reset}  $1${reset}"
  
  msg_colour_simple "title" "Final tasks to setup mouNTain:"
  msg_colour_simple "info-bold" "(1) Enable sudo to run mouNTain from any folder:"
  msg_colour_simple "info"  "$ sudo visudo"
  msg_colour_simple "info"  ".. add/edit line: 'Defaults secure_path:<existing_path>:/path/to/mouNTain'"
  printf "%s\n"
  msg_colour_simple "info-bold"  "(2) Run mouNTain help for usage instructions:"
  msg_colour_simple "info"  "$ sudo mountain -h"
  printf "%s\n"

elif [[ ${os} == "Centos" ]]; then

  msg_colour_simple "alert" "Getting yum package manager update"
  printf "%s\t\t" "$ sudo yum install -y update"
  sudo yum install -y update > /dev/null 2>&1
  printf "%s\n" "${green}${b} ✓${reset}  $1${reset}"
  msg_colour_simple "alert" "Installing sshfs package"
  printf "%s\t\t" "$ sudo yum install -y install sshfs"
  sudo yum install -y sshfs
  printf "%s\n" "${green}${b} ✓${reset}  $1${reset}"
  msg_colour_simple "alert" "Installing arp-scan"
  printf "%s\t"   "$ sudo yum install -y install arp-scan"
  sudo yum install -y arp-scan
  printf "%s\n\n" "${green}${b} ✓${reset}  $1${reset}"

  msg_colour_simple "title" "Final tasks to setup mouNTain:"
  msg_colour_simple "info-bold" "(1) Enable sudo to run mouNTain from any folder:"
  msg_colour_simple "info"  "$ sudo visudo"
  msg_colour_simple "info"  ".. add/edit line: 'Defaults secure_path:<existing_path>:/path/to/mouNTain'"
  printf "%s\n"
  msg_colour_simple "info-bold"  "(2) Run mouNTain help for usage instructions:"
  msg_colour_simple "info"  "$ sudo mountain -h"
  printf "%s\n"

elif [[ ${os} == "Redhat" ]]; then

  msg_colour_simple "alert" "Getting yum package manager update"
  printf "%s\t\t" "$ sudo yum install -y update"
  sudo yum install -y update > /dev/null 2>&1
  printf "%s\n" "${green}${b} ✓${reset}  $1${reset}"
  msg_colour_simple "alert" "Installing sshfs package"
  printf "%s\t\t" "$ sudo yum install -y install sshfs"
  sudo yum install -y sshfs
  printf "%s\n" "${green}${b} ✓${reset}  $1${reset}"
  msg_colour_simple "alert" "Installing arp-scan"
  printf "%s\t"   "$ sudo yum install -y install arp-scan"
  sudo yum install -y arp-scan
  printf "%s\n\n" "${green}${b} ✓${reset}  $1${reset}"

  msg_colour_simple "title" "Final tasks to setup mouNTain:"
  msg_colour_simple "info-bold" "(1) Enable sudo to run mouNTain from any folder:"
  msg_colour_simple "info"  "$ sudo visudo"
  msg_colour_simple "info"  ".. add/edit line: 'Defaults secure_path:<existing_path>:/path/to/mouNTain'"
  printf "%s\n"
  msg_colour_simple "info-bold"  "(2) Run mouNTain help for usage instructions:"
  msg_colour_simple "info"  "$ sudo mountain -h"
  printf "%s\n"

else

  clear
  msg_mnt_osNotSupported
  exit 1;

fi
